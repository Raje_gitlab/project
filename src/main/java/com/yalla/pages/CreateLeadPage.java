package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations{ 
	
	public CreateLeadPage() {
       PageFactory.initElements(driver, this);
       
	} 

	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFirstName;	
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLastName;	
	@FindBy(how=How.XPATH, using="//input[@class='smallSubmit']") WebElement eleCreateLead;	
	
	@And("enter company name as (.*)")
	public CreateLeadPage enterCompanyName(String data) {
		clearAndType(eleCompanyName, data);  
		return this;
	}
	
	@And("enter first name as (.*)")
	public CreateLeadPage enterFirstName(String data) {
		clearAndType(eleFirstName, data);  
		return this;
	}
	
	@And("enter last name as (.*)")
	public CreateLeadPage enterLastName(String data) {
		clearAndType(eleLastName, data);  
		return this;
	}
	
	@When("click on create leaad")
	public ViewLeadPage clickCreateLead() {
		click(eleCreateLead);  
		return new ViewLeadPage();
	}

	
}







