package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class MyLeadsPage extends Annotations{ 

	public MyLeadsPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement eleCreateLead;
	@FindBy(how=How.LINK_TEXT, using="Find Leads") WebElement eleFindLead;
	
	@And("click on Create Lead")
	public CreateLeadPage clickCreateLeads() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleCreateLead);  
		return new CreateLeadPage();
	}
	
	public FindLeadsPage clickFindLeads() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleFindLead);  
		return new FindLeadsPage();
	}
}







