package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class OpenTapsCRMPage extends Annotations{ 

	public OpenTapsCRMPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.ID, using="updateLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.XPATH, using="(//input[@class='smallSubmit'])[1]") WebElement eleUpdate;	
	
	public OpenTapsCRMPage enterCompanyName(String cName) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		clearAndType(eleCompanyName,cName);  
		return this;
	}
	
	public ViewLeadPage clickUpdate() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleUpdate); 
		return new ViewLeadPage();
	}
	
	
}







