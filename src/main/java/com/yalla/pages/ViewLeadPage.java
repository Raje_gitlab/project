package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Then;

public class ViewLeadPage extends Annotations{ 

	public ViewLeadPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.ID, using="viewLead_firstName_sp") WebElement eleFirstName;
	@FindBy(how=How.ID, using="viewLead_companyName_sp") WebElement eleCompanyName;
	@FindBy(how=How.LINK_TEXT, using="Edit") WebElement eleEdit;	
	@FindBy(how=How.LINK_TEXT, using="Delete") WebElement eleDelete;	
	
	//@Then()
	public ViewLeadPage verifyFirstName(String fName) {
		verifyExactText(eleFirstName,fName);  
		return this;
	}
	
	public ViewLeadPage verifyCompanyName(String cName) {
		verifyPartialText(eleCompanyName,cName);  
		return this;
	}
		
	public OpenTapsCRMPage clickEdit() {
		click(eleEdit); 
		return new OpenTapsCRMPage();
	}
	
	public MyLeadsPage clickDelete() {
		click(eleDelete); 
		return new MyLeadsPage();
	}
	
	@Then("verify lead created succesfully")
	public ViewLeadPage verifyTitle() {
		verifyTitle("View Lead");
		return this;
	}
	
}







